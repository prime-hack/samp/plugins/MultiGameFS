#pragma once

#include <string>
#include <string_view>
#include <fstream>
#include <memory>
#include <filesystem>

class MGFSLoader {
	static constexpr const wchar_t *kMpLibNames[] = { L"samp.dll",		 L"crmp.dll",	   L"vcmp-proxy.dll",
													  L"vcmp-steam.dll", L"vcmp-game.dll", L"lu.dll" };

	std::ofstream out_;
	std::unique_ptr<class Config> config_;
	std::unique_ptr<class MultiGameFS> mgfs_;

public:
	MGFSLoader() noexcept;
	~MGFSLoader();

protected:
	std::string server_;
	std::filesystem::path mplibName_;
	void *mplib_ = (void *)(std::size_t)-1;

	void detectServer() noexcept;
	std::string getMPName() noexcept;
	static std::string_view findArgument( std::string_view key ) noexcept;
	static bool makePath( const std::filesystem::path &path );
	bool restartGame( const std::filesystem::path &fs );
	std::filesystem::path findInMod( const std::filesystem::path &modpath, std::wstring_view filename ) noexcept;
	void loadAsiPlugins( const std::filesystem::path &modpath );
	static bool inject( size_t pId, const wchar_t *dllname );
	static std::filesystem::path getModulePath( std::wstring_view module = L"" );

	std::ostream &log();
};
