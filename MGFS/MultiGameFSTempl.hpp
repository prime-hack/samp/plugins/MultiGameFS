#pragma once

#include "MGFS/MultiGameFS.h"

#include <type_traits>
#include <iomanip>

#include <llmo/detour.h>
#include <llmo/fn.h>

#include <windows.h>
#include <assert.h>

template<typename CharType> class MultiGameFSTempl {
	friend class MultiGameFS;

	std::unique_ptr<llmo::hook::detour::hook_t<HANDLE( const CharType *, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE )>>
		CreateFile_;
	std::unique_ptr<llmo::hook::detour::hook_t<DWORD( const CharType * )>> GetFileAttributes_;
	std::unique_ptr<llmo::hook::detour::hook_t<DWORD( const CharType * )>> AddFontResource_;
	std::unique_ptr<llmo::hook::detour::hook_t<DWORD( const CharType *, DWORD, PVOID )>> AddFontResourceEx_;
	std::unique_ptr<llmo::hook::detour::hook_t<HMODULE( const CharType * )>> LoadLibrary_;
	std::unique_ptr<llmo::hook::detour::hook_t<HANDLE( const CharType *, void * )>> FindFirstFile_;
	std::unique_ptr<llmo::hook::detour::hook_t<HANDLE( const CharType *, FINDEX_INFO_LEVELS, LPVOID, FINDEX_SEARCH_OPS, LPVOID, DWORD )>>
		FindFirstFileEx_;
	std::unique_ptr<
		llmo::hook::detour::hook_t<HANDLE( const CharType *, FINDEX_INFO_LEVELS, LPVOID, FINDEX_SEARCH_OPS, LPVOID, DWORD, HANDLE )>>
		FindFirstFileTransacted_;
	std::unique_ptr<llmo::hook::detour::hook_t<FILE *( const CharType *, const CharType * )>> fopen_;
	std::unique_ptr<llmo::hook::detour::hook_t<errno_t( FILE **, const CharType *, const CharType * )>> fopen_s_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType *, LPSECURITY_ATTRIBUTES )>> CreateDirectory_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType *, GET_FILEEX_INFO_LEVELS, LPVOID )>> GetFileAttributesEx_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType *, GET_FILEEX_INFO_LEVELS, LPVOID, HANDLE )>>
		GetFileAttributesTransacted_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType *, const CharType *, BOOL )>> CopyFile_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType * )>> DeleteFile_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType *, HANDLE )>> DeleteFileTransacted_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType * )>> RemoveDirectory_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType *, HANDLE )>> RemoveDirectoryTransacted_;
	std::unique_ptr<llmo::hook::detour::hook_t<BOOL( const CharType * )>> SetCurrentDirectory_;

	/// папка с игрой
	std::basic_string<CharType> root_;
	/// текущая папка
	std::basic_string<CharType> folder_;
	/// Путь к виртуальной текущей папке
	std::basic_string<CharType> currentFolder_;
	/// редиректы имен файлов
	std::map<std::basic_string<CharType>, std::basic_string<CharType>> fileRedirects_;
	/// альтернативные пути в формате юникода
	std::vector<std::basic_string<CharType>> altPaths_;
	/// трассировка вызовов
	bool trace_ = false;

public:
	MultiGameFSTempl( std::ostream &out,
					  std::basic_string_view<CharType> root,
					  std::ostream &out_trace,
					  void *ctxMem,
					  bool trace,
					  const std::vector<std::string> &alt_paths ) :
		out_( out ),
		root_( root ), out_trace_( out_trace ), trace_( trace ) {
		currentFolder_ = root_;
		for ( auto &&alt_path : alt_paths ) {
			if constexpr ( std::is_same<CharType, char>::value )
				altPaths_.emplace_back( alt_path );
			else
				altPaths_.emplace_back( MultiGameFS::toWstring( alt_path ) );
		}

		enum class eCallType : bool {
			stdcall = false,
			ccall
		};
		auto install_hook = [&]( auto &detour, void *method, const char *libraryName, const char *ansiName, const char *wcharName, int args, int ctxOffset = 0, eCallType callType = eCallType::stdcall ) {
			auto library = GetModuleHandleA( libraryName ); // This function can be missed on some plathorms
			void *fnPtr;
			if constexpr ( std::is_same<CharType, char>::value )
				fnPtr = (void *)GetProcAddress( library, ansiName );
			else
				fnPtr = (void *)GetProcAddress( library, wcharName );
			if ( !fnPtr ) return 0;

			void *ctx;
			if ( callType == eCallType::ccall )
				ctx = llmo::fn::contextCCall( this, method, args, (std::uint8_t *)ctxMem + ctxOffset );
			else
				ctx = llmo::fn::contextStdCallCopyArgs( this, method, args, (std::uint8_t *)ctxMem + ctxOffset );
			using hook_t = typename std::remove_pointer<decltype( detour.get() )>::type;
			detour = std::make_unique<hook_t>( fnPtr, ctx );

			auto szCallType = ( callType == eCallType::ccall ? "__cdecl " : "__stdcall " );
			if constexpr ( std::is_same<CharType, char>::value )
				log() << "Install " << szCallType << ansiName << " hook at " << std::hex << fnPtr << std::endl;
			else
				log() << "Install " << szCallType << wcharName << " hook at " << std::hex << fnPtr << std::endl;

			return ( callType == eCallType::ccall ? 21 : 23 ) + args * 5 + 1;
		};

		auto offset = install_hook( CreateFile_, llmo::fn::fn2void( &MultiGameFSTempl::CreateFileHook ), "kernel32", "CreateFileA", "CreateFileW", 7 );
		offset += install_hook( GetFileAttributes_, llmo::fn::fn2void( &MultiGameFSTempl::GetFileAttributesHook ), "kernel32", "GetFileAttributesA", "GetFileAttributesW", 1, offset );
		offset += install_hook( AddFontResource_, llmo::fn::fn2void( &MultiGameFSTempl::AddFontResourceHook ), "kernel32", "AddFontResourceA", "AddFontResourceW", 1, offset );
		offset += install_hook( AddFontResourceEx_, llmo::fn::fn2void( &MultiGameFSTempl::AddFontResourceExHook ), "kernel32", "AddFontResourceExA", "AddFontResourceExW", 3, offset );
		offset += install_hook( LoadLibrary_, llmo::fn::fn2void( &MultiGameFSTempl::LoadLibraryHook ), "kernel32", "LoadLibraryA", "LoadLibraryW", 1, offset );
		offset += install_hook( FindFirstFile_, llmo::fn::fn2void( &MultiGameFSTempl::FindFirstFileHook ), "kernel32", "FindFirstFileA", "FindFirstFileW", 2, offset );
		offset += install_hook( FindFirstFileEx_, llmo::fn::fn2void( &MultiGameFSTempl::FindFirstFileExHook ), "kernel32", "FindFirstFileExA", "FindFirstFileExW", 6, offset );
		offset += install_hook( FindFirstFileTransacted_, llmo::fn::fn2void( &MultiGameFSTempl::FindFirstFileTransactedHook ), "kernel32", "FindFirstFileTransactedA", "FindFirstFileTransactedW", 7, offset );
		offset += install_hook( fopen_, llmo::fn::fn2void( &MultiGameFSTempl::fopenHook ), "msvcrt", "fopen", "_wfopen", 2, offset, eCallType::ccall );
		offset += install_hook( fopen_s_, llmo::fn::fn2void( &MultiGameFSTempl::fopen_sHook ), "msvcrt", "fopen_s", "_wfopen_s", 3, offset, eCallType::ccall );
		offset += install_hook( CreateDirectory_, llmo::fn::fn2void( &MultiGameFSTempl::CreateDirectoryHook ), "kernel32", "CreateDirectoryA", "CreateDirectoryW", 2, offset );
		offset += install_hook( GetFileAttributesEx_, llmo::fn::fn2void( &MultiGameFSTempl::GetFileAttributesExHook ), "kernel32", "GetFileAttributesExA", "GetFileAttributesExW", 3, offset );
		offset += install_hook( GetFileAttributesTransacted_, llmo::fn::fn2void( &MultiGameFSTempl::GetFileAttributesTransactedHook ), "kernel32", "GetFileAttributesTransactedA", "GetFileAttributesTransactedW", 4, offset );
		offset += install_hook( CopyFile_, llmo::fn::fn2void( &MultiGameFSTempl::CopyFileHook ), "kernel32", "CopyFileA", "CopyFileW", 3, offset );
		offset += install_hook( DeleteFile_, llmo::fn::fn2void( &MultiGameFSTempl::DeleteFileHook ), "kernel32", "DeleteFileA", "DeleteFileW", 1, offset );
		offset += install_hook( DeleteFileTransacted_, llmo::fn::fn2void( &MultiGameFSTempl::DeleteFileTransactedHook ), "kernel32", "DeleteFileTransactedA", "DeleteFileTransactedW", 2, offset );
		offset += install_hook( RemoveDirectory_, llmo::fn::fn2void( &MultiGameFSTempl::RemoveDirectoryHook ), "kernel32", "RemoveDirectoryA", "RemoveDirectoryW", 1, offset );
		offset += install_hook( RemoveDirectoryTransacted_, llmo::fn::fn2void( &MultiGameFSTempl::RemoveDirectoryTransactedHook ), "kernel32", "RemoveDirectoryTransactedA", "RemoveDirectoryTransactedW", 2, offset );
		offset += install_hook( SetCurrentDirectory_, llmo::fn::fn2void( &MultiGameFSTempl::SetCurrentDirectoryHook ), "kernel32", "SetCurrentDirectoryA", "SetCurrentDirectoryW", 1, offset );
	}

protected:
	std::ostream &out_;
	std::ostream &out_trace_;

	void setNameRedirect( const CharType *orig, const CharType *alt ) { fileRedirects_[orig] = alt; }

	using SECATTRIB = LPSECURITY_ATTRIBUTES;
	HANDLE CreateFileHook( const CharType *file,
						   DWORD dwDesiredAccess,
						   DWORD a3,
						   SECATTRIB a4,
						   DWORD dwCreationDisposition,
						   DWORD dwFlagsAndAttributes,
						   HANDLE a7 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "CreateFileA( \"" << file << "\", " << std::hex << dwDesiredAccess << ", " << a3 << ", " << a4 << ", "
						   << dwCreationDisposition << ", " << std::hex << dwFlagsAndAttributes << ", " << a7 << " )" << std::endl;
			else
				out_trace_ << "CreateFileW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << dwDesiredAccess << ", " << a3
						   << ", " << a4 << ", " << dwCreationDisposition << ", " << std::hex << dwFlagsAndAttributes << ", " << a7 << " )"
						   << std::endl;
		}

		bool allowCreateFile = false;
		if ( ( dwDesiredAccess & GENERIC_WRITE ) == GENERIC_WRITE &&
			 ( dwCreationDisposition == CREATE_NEW || dwCreationDisposition == CREATE_ALWAYS ) &&
			 !( dwFlagsAndAttributes & FILE_ATTRIBUTE_READONLY ) && !( dwFlagsAndAttributes & FILE_FLAG_DELETE_ON_CLOSE ) )
			allowCreateFile = true;

		return processHook<HANDLE>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) {
				return MultiGameFS::stdCallOriginal( CreateFile_->original,
													 filepath,
													 dwDesiredAccess,
													 a3,
													 a4,
													 dwCreationDisposition,
													 dwFlagsAndAttributes,
													 a7 );
			},
			[]( HANDLE res ) { return res; },
			allowCreateFile ? "new file" : "file",
			allowCreateFile );
	}
	DWORD GetFileAttributesHook( const CharType *file ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "GetFileAttributesA( \"" << file << "\" )" << std::endl;
			else
				out_trace_ << "GetFileAttributesW( \"" << MultiGameFS::toString( file ) << "\" )" << std::endl;
		}

		return processHook<DWORD>(
			getFullPath( file ).c_str(),
			[this]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( GetFileAttributes_->original, filepath ); },
			[]( DWORD res ) { return res != INVALID_FILE_ATTRIBUTES; },
			"attribute" );
	}
	DWORD AddFontResourceHook( const CharType *file ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "AddFontResourceA( \"" << file << "\" )" << std::endl;
			else
				out_trace_ << "AddFontResourceW( \"" << MultiGameFS::toString( file ) << "\" )" << std::endl;
		}

		return processHook<DWORD>(
			getFullPath( file ).c_str(),
			[this]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( AddFontResource_->original, filepath ); },
			[]( DWORD res ) { return res; },
			"font" );
	}
	DWORD AddFontResourceExHook( const CharType *file, DWORD flags, PVOID res ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "AddFontResourceExA( \"" << file << "\", " << std::hex << flags << ", " << std::hex << res << " )"
						   << std::endl;
			else
				out_trace_ << "AddFontResourceExW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << flags << ", " << std::hex
						   << res << " )" << std::endl;
		}

		return processHook<DWORD>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( AddFontResourceEx_->original, filepath, flags, res ); },
			[]( DWORD res ) { return res; },
			"font" );
	}
	HMODULE LoadLibraryHook( const CharType *file ) { // TODO: fix library loading
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "LoadLibraryA( \"" << file << "\" )" << std::endl;
			else
				out_trace_ << "LoadLibraryW( \"" << MultiGameFS::toString( file ) << "\" )" << std::endl;
		}

		return processHook<HMODULE>(
			file,
			[this]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( LoadLibrary_->original, filepath ); },
			[]( HMODULE res ) { return res && res != INVALID_HANDLE_VALUE; },
			"library" );
	}
	HANDLE FindFirstFileHook( const CharType *file, void *a2 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "FindFirstFileA( \"" << file << "\", " << std::hex << a2 << " )" << std::endl;
			else
				out_trace_ << "FindFirstFileW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << a2 << " )" << std::endl;
		}

		return processIterHook( getFullPath( file ).c_str(), [&]( const CharType *filepath ) {
			return MultiGameFS::stdCallOriginal( FindFirstFile_->original, filepath, a2 );
		} );
	}
	HANDLE FindFirstFileExHook( const CharType *file, FINDEX_INFO_LEVELS a2, LPVOID a3, FINDEX_SEARCH_OPS a4, LPVOID a5, DWORD a6 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "FindFirstFileExA( \"" << file << "\", " << std::hex << (void *)a2 << ", " << std::hex << (void *)a3 << ", "
						   << std::hex << (void *)a4 << ", " << std::hex << (void *)a5 << ", " << std::hex << (void *)a6 << " )"
						   << std::endl;
			else
				out_trace_ << "FindFirstFileExW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << (void *)a2 << ", "
						   << std::hex << (void *)a3 << ", " << std::hex << (void *)a4 << ", " << std::hex << (void *)a5 << ", " << std::hex
						   << (void *)a6 << " )" << std::endl;
		}

		return processIterHook( getFullPath( file ).c_str(), [&]( const CharType *filepath ) {
			return MultiGameFS::stdCallOriginal( FindFirstFileEx_->original, filepath, a2, a3, a4, a5, a6 );
		} );
	}
	HANDLE
	FindFirstFileTransactedHook( const CharType *file,
								 FINDEX_INFO_LEVELS a2,
								 LPVOID a3,
								 FINDEX_SEARCH_OPS a4,
								 LPVOID a5,
								 DWORD a6,
								 HANDLE a7 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "FindFirstFileTransactedA( \"" << file << "\", " << std::hex << (void *)a2 << ", " << std::hex << (void *)a3
						   << ", " << std::hex << (void *)a4 << ", " << std::hex << (void *)a5 << ", " << std::hex << (void *)a6 << ", "
						   << std::hex << (void *)a7 << " )" << std::endl;
			else
				out_trace_ << "FindFirstFileTransactedW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << (void *)a2 << ", "
						   << std::hex << (void *)a3 << ", " << std::hex << (void *)a4 << ", " << std::hex << (void *)a5 << ", " << std::hex
						   << (void *)a6 << ", " << std::hex << (void *)a7 << " )" << std::endl;
		}

		return processIterHook( getFullPath( file ).c_str(), [&]( const CharType *filepath ) {
			return MultiGameFS::stdCallOriginal( FindFirstFileTransacted_->original, filepath, a2, a3, a4, a5, a6, a7 );
		} );
	}
	FILE *fopenHook( const CharType *file, const CharType *mode ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "fopen( \"" << file << "\", \"" << mode << "\" )" << std::endl;
			else
				out_trace_ << "_wfopen( \"" << MultiGameFS::toString( file ) << "\", \"" << MultiGameFS::toString( mode ) << "\" )"
						   << std::endl;
		}

		bool allowCreateFile = false;
		if constexpr ( std::is_same<CharType, char>::value ) {
			if ( mode && ( std::strstr( mode, "w" ) || std::strstr( mode, "W" ) ) ) allowCreateFile = true;
		} else {
			if ( mode && ( std::wcsstr( mode, L"w" ) || std::wcsstr( mode, L"W" ) ) ) allowCreateFile = true;
		}

		return processHook<FILE *>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) { return fopen_->original( filepath, mode ); },
			[]( FILE *res ) { return res; },
			allowCreateFile ? "new file" : "file",
			allowCreateFile );
	}
	errno_t fopen_sHook( FILE **pFile, const CharType *file, const CharType *mode ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "fopen( \"" << file << "\", \"" << mode << "\" )" << std::endl;
			else
				out_trace_ << "_wfopen( \"" << MultiGameFS::toString( file ) << "\", \"" << MultiGameFS::toString( mode ) << "\" )"
						   << std::endl;
		}

		bool allowCreateFile = false;
		if constexpr ( std::is_same<CharType, char>::value ) {
			if ( mode && ( std::strstr( mode, "w" ) || std::strstr( mode, "W" ) ) ) allowCreateFile = true;
		} else {
			if ( mode && ( std::wcsstr( mode, L"w" ) || std::wcsstr( mode, L"W" ) ) ) allowCreateFile = true;
		}

		return processHook<errno_t>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) { return fopen_s_->original( pFile, filepath, mode ); },
			[]( errno_t res ) { return res != 0; },
			allowCreateFile ? "new file" : "file",
			allowCreateFile );
	}
	BOOL CreateDirectoryHook( const CharType *file, SECATTRIB a2 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "CreateDirectoryA( \"" << file << "\", " << std::hex << a2 << " )" << std::endl;
			else
				out_trace_ << "CreateDirectoryW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << a2 << " )" << std::endl;
		}

		auto result = processHook<BOOL>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) {
				auto result = MultiGameFS::stdCallOriginal( CreateDirectory_->original, filepath, a2 );
				if ( !result && GetLastError() == ERROR_ALREADY_EXISTS ) result = TRUE;
				return result;
			},
			[]( BOOL res ) { return res; },
			"new directory",
			true );
		return result;
	}
	BOOL GetFileAttributesExHook( const CharType *file, GET_FILEEX_INFO_LEVELS a2, LPVOID a3 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "GetFileAttributesExA( \"" << file << "\", " << std::hex << a2 << ", " << std::hex << a3 << " )" << std::endl;
			else
				out_trace_ << "GetFileAttributesExW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << a2 << ", " << std::hex
						   << a3 << " )" << std::endl;
		}

		return processHook<BOOL>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( GetFileAttributesEx_->original, filepath, a2, a3 ); },
			[]( BOOL res ) { return res; },
			"attribute",
			true );
	}
	BOOL GetFileAttributesTransactedHook( const CharType *file, GET_FILEEX_INFO_LEVELS a2, LPVOID a3, HANDLE a4 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "GetFileAttributesTransactedA( \"" << file << "\", " << std::hex << a2 << ", " << std::hex << a3 << ", "
						   << std::hex << a4 << " )" << std::endl;
			else
				out_trace_ << "GetFileAttributesTransactedW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << a2 << ", "
						   << std::hex << a3 << ", " << std::hex << a4 << " )" << std::endl;
		}

		return processHook<BOOL>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) {
				return MultiGameFS::stdCallOriginal( GetFileAttributesTransacted_->original, filepath, a2, a3, a4 );
			},
			[]( BOOL res ) { return res; },
			"attribute",
			true );
	}
	BOOL CopyFileHook( const CharType *src, const CharType *dst, BOOL a3 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "CopyFileA( \"" << src << "\", \"" << dst << "\", " << std::boolalpha << a3 << " )" << std::endl;
			else
				out_trace_ << "CopyFileW( \"" << MultiGameFS::toString( src ) << "\", \"" << MultiGameFS::toString( dst ) << "\", "
						   << std::boolalpha << a3 << " )" << std::endl;
		}

		auto full_src = getFullPath( src );
		auto full_dst = getFullPath( src );

		auto alt_src = getAltPath( full_src.c_str() );
		if ( alt_src.empty() ) return MultiGameFS::stdCallOriginal( CopyFile_->original, full_src.c_str(), full_dst.c_str(), a3 );

		auto alt_dst = getAltPath( full_dst.c_str(), true );
		if ( alt_dst.empty() ) alt_dst = full_dst;
		auto result = MultiGameFS::stdCallOriginal( CopyFile_->original, alt_src.c_str(), alt_dst.c_str(), a3 );


		std::string alt_src_ansi, rel_src_ansi, alt_dst_ansi, rel_dst_ansi;
		if constexpr ( std::is_same<CharType, char>::value ) {
			alt_src_ansi = getRelPath( alt_src.c_str() );
			rel_src_ansi = getRelPath( full_src.c_str() );
			alt_dst_ansi = getRelPath( alt_dst.c_str() );
			rel_dst_ansi = getRelPath( full_dst.c_str() );
		} else {
			alt_src_ansi = MultiGameFS::toString( getRelPath( alt_src.c_str() ) );
			rel_src_ansi = MultiGameFS::toString( getRelPath( full_src.c_str() ) );
			alt_dst_ansi = MultiGameFS::toString( getRelPath( alt_dst.c_str() ) );
			rel_dst_ansi = MultiGameFS::toString( getRelPath( full_dst.c_str() ) );
		}

		if ( result ) {
			log() << "redirect copy \"" << rel_src_ansi << ":" << rel_dst_ansi << "\" to \"" << alt_src_ansi << ":" << alt_dst_ansi << "\""
				  << std::endl;
		} else {
			result = MultiGameFS::stdCallOriginal( CopyFile_->original, full_src.c_str(), full_dst.c_str(), a3 );
			if ( result ) {
				log() << "failed redirect copy \"" << rel_src_ansi << ":" << rel_dst_ansi << "\" to \"" << alt_src_ansi << ":"
					  << alt_dst_ansi << "\"!" << std::endl;
			}
		}

		return result;
	}
	BOOL DeleteFileHook( const CharType *file ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "DeleteFileA( \"" << file << "\" )" << std::endl;
			else
				out_trace_ << "DeleteFileW( \"" << MultiGameFS::toString( file ) << "\" )" << std::endl;
		}

		return processHook<BOOL>(
			getFullPath( file ).c_str(),
			[this]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( DeleteFile_->original, filepath ); },
			[]( BOOL res ) { return res; },
			"delete file" );
	}
	BOOL DeleteFileTransactedHook( const CharType *file, HANDLE a2 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "DeleteFileTransactedA( \"" << file << "\", " << std::hex << a2 << " )" << std::endl;
			else
				out_trace_ << "DeleteFileTransactedW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << a2 << " )" << std::endl;
		}

		return processHook<BOOL>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( DeleteFileTransacted_->original, filepath, a2 ); },
			[]( BOOL res ) { return res; },
			"delete file" );
	}
	BOOL RemoveDirectoryHook( const CharType *file ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "RemoveDirectoryA( \"" << file << "\" )" << std::endl;
			else
				out_trace_ << "RemoveDirectoryW( \"" << MultiGameFS::toString( file ) << "\" )" << std::endl;
		}

		return processHook<BOOL>(
			getFullPath( file ).c_str(),
			[this]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( RemoveDirectory_->original, filepath ); },
			[]( BOOL res ) { return res; },
			"delete directory" );
	}
	BOOL RemoveDirectoryTransactedHook( const CharType *file, HANDLE a2 ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "RemoveDirectoryTransactedA( \"" << file << "\", " << std::hex << a2 << " )" << std::endl;
			else
				out_trace_ << "RemoveDirectoryTransactedW( \"" << MultiGameFS::toString( file ) << "\", " << std::hex << a2 << " )"
						   << std::endl;
		}

		return processHook<BOOL>(
			getFullPath( file ).c_str(),
			[&]( const CharType *filepath ) { return MultiGameFS::stdCallOriginal( RemoveDirectoryTransacted_->original, filepath, a2 ); },
			[]( BOOL res ) { return res; },
			"delete directory" );
	}
	BOOL SetCurrentDirectoryHook( const CharType *file ) {
		if ( trace_ ) {
			if constexpr ( std::is_same<CharType, char>::value )
				out_trace_ << "SetCurrentDirectoryA( \"" << file << "\" )" << std::endl;
			else
				out_trace_ << "SetCurrentDirectoryW( \"" << MultiGameFS::toString( file ) << "\" )" << std::endl;
		}

		currentFolder_.clear();

		auto result = MultiGameFS::stdCallOriginal( SetCurrentDirectory_->original, file );
		if ( result ) return result;

		auto alt_path = getAltPath( file );
		if ( alt_path.empty() ) return result;

		if ( file && *file && file[1] == sym::colon() )
			currentFolder_ = file;
		else {
			currentFolder_ = getCurrentPath();
			if ( !currentFolder_.empty() && currentFolder_.back() != sym::backslash() && currentFolder_.back() != sym::slash() )
				currentFolder_.push_back( sym::backslash() );
			currentFolder_ += file;
		}
		if ( !currentFolder_.empty() && currentFolder_.back() != sym::backslash() && currentFolder_.back() != sym::slash() )
			currentFolder_.push_back( sym::backslash() );

		return TRUE;
	}

	std::ostream &log() { return out_ << "MultiGameFS: "; }

private:
	/**
	 * @brief Получает путь относительно папки игры
	 * @param fullPath полный путь
	 * @return путь относительно папки с игрой. Никогда не возвращает nullptr
	 */
	const CharType *getRelPath( const CharType *fullPath ) {
		if ( !fullPath ) return fullPath;
		auto relPath = fullPath;
		if ( strstr( fullPath, root_.c_str() ) == fullPath ) {
			relPath = &fullPath[root_.length()];
			if ( *relPath == sym::backslash() || *relPath == sym::slash() ) relPath++;
		}

		return relPath;
	}
	/**
	 * @brief Получает альтернативный путь для файла
	 * @param file путь к оригинальному файлу
	 * @param allowCreate разрешить создание. Если разрешено, то альтернативный путь возвращается если каталог существует
	 * @return Альтернатинвый путь. Если файл не найден в альтернативных местах, то возвращается оригинальный путь
	 */
	std::basic_string<CharType> getRedirect( const CharType *file, bool allowCreate = false ) {
		if ( isAlreadyRedirected( file ) ) return file;
		if ( !file ) return file;
		updateFolder();
		if ( folder_.length() >= 2 && folder_[1] == sym::colon() ) return file;

		std::basic_string<CharType> filePath = file;
		if ( !folder_.empty() ) filePath = folder_ + filePath;
		while ( filePath.back() == sym::space() || filePath.back() == sym::tab() ) filePath.pop_back();

		std::basic_string<CharType> root = root_;
		if ( root.back() != sym::backslash() && root.back() != sym::slash() ) root.push_back( sym::backslash() );

		for ( auto &&alt_path : altPaths_ ) {
			std::basic_string<CharType> path = root + alt_path;
			if ( fileRedirects_.contains( filePath ) )
				path += fileRedirects_[filePath];
			else
				path += filePath;
			if ( MultiGameFS::stdCallOriginal( GetFileAttributes_->original, path.c_str() ) != INVALID_FILE_ATTRIBUTES )
				return path;
			else if ( allowCreate ) {
				auto slashPos = path.rfind( sym::slash() );
				auto backslashPos = path.rfind( sym::backslash() );
				size_t endDir;
				if ( slashPos != std::string::npos && backslashPos != std::string::npos )
					endDir = std::max( slashPos, backslashPos );
				else if ( slashPos != std::string::npos )
					endDir = slashPos;
				else
					endDir = backslashPos;
				auto dir = path.substr( 0, endDir );
				if ( MultiGameFS::stdCallOriginal( GetFileAttributes_->original, dir.c_str() ) != INVALID_FILE_ATTRIBUTES ) return path;
			}
		}

		return file;
	}
	/**
	 * @brief Находит альтернативные пути и возвращает первый найденный, если путь валиден
	 * @param file Файл для которого ищется альтернатива
	 * @param allowCreate разрешить создание. Если разрешено, то альтернативный путь возвращается если каталог существует
	 * @return Пустая строка, если альтернативы нет. Иначе путь к альтернаивному файлу
	 */
	std::basic_string<CharType> getAltPath( const CharType *file, bool allowCreate = false ) {
		if ( isAlreadyRedirected( file ) ) return std::basic_string<CharType>();
		if constexpr ( std::is_same<CharType, char>::value ) {
			if ( file && ( !std::strcmp( file, "." ) || !std::strcmp( file, ".." ) || !std::strcmp( file, "CONIN$" ) ||
						   !std::strcmp( file, "CONOUT$" ) || !std::strcmp( file, "NUL" ) ) )
				return std::basic_string<CharType>();
		} else {
			if ( file && ( !std::wcscmp( file, L"." ) || !std::wcscmp( file, L".." ) || !std::wcscmp( file, L"CONIN$" ) ||
						   !std::wcscmp( file, L"CONOUT$" ) || !std::wcscmp( file, L"NUL" ) ) )
				return std::basic_string<CharType>();
		}
		auto relPath = getRelPath( file );
		if ( relPath[0] == sym::null() || relPath[1] == sym::colon() ) return std::basic_string<CharType>();
		auto alt_file = getRedirect( relPath, allowCreate );
		if ( alt_file.length() >= 2 && alt_file[1] != sym::colon() ) return std::basic_string<CharType>();
		if constexpr ( std::is_same<CharType, char>::value ) {
			if ( alt_file.ends_with( "\\." ) || alt_file.ends_with( "/." ) || alt_file.ends_with( "\\.." ) || alt_file.ends_with( "/.." ) )
				return std::basic_string<CharType>();
		} else {
			if ( alt_file.ends_with( L"\\." ) || alt_file.ends_with( L"/." ) || alt_file.ends_with( L"\\.." ) ||
				 alt_file.ends_with( L"/.." ) )
				return std::basic_string<CharType>();
		}

		return alt_file;
	}
	/**
	 * @brief Находит путь поиска файлов
	 * @param path Путь поиска
	 * @return Путь поиска файлов
	 */
	std::basic_string<CharType> getSearchPathForIter( const CharType *path ) {
		auto relPath = getRelPath( path );
		if ( relPath && *relPath && relPath[1] == sym::colon() ) return path;
		updateFolder();
		if ( folder_.length() >= 2 && folder_[1] == sym::colon() ) return path;

		std::basic_string<CharType> filePath = relPath;
		if ( !folder_.empty() ) filePath = folder_ + filePath;
		while ( filePath.back() == sym::space() || filePath.back() == sym::tab() ) filePath.pop_back();

		return filePath;
	}
	/**
	 * @brief Получает путь к текущей директории
	 * @return Пустая строка в случае ошибки, иначе путь к текущей директории
	 */
	std::basic_string<CharType> getCurrentPath() {
		CharType buffer[0x400];
		if constexpr ( std::is_same<CharType, char>::value ) {
			if ( !GetCurrentDirectoryA( sizeof( buffer ) - sizeof( CharType ), buffer ) ) return std::basic_string<CharType>();
		} else {
			if ( !GetCurrentDirectoryW( sizeof( buffer ) - sizeof( CharType ), buffer ) ) return std::basic_string<CharType>();
		}
		buffer[( sizeof( buffer ) / sizeof( CharType ) ) - 1] = sym::null();
		return buffer;
	}
	/**
	 * @brief Обновляет путь к текущей папке
	 */
	void updateFolder() {
		auto fullPath = getCurrentPath();
		auto folder = getRelPath( fullPath.c_str() );
		if ( folder && *folder && folder[1] == sym::colon() && strstr( root_.c_str(), folder ) == root_.c_str() ) {
			folder_.clear();
		} else {
			if ( folder ) {
				folder_.assign( folder );
				if ( !folder_.empty() && folder_.back() != sym::backslash() && folder_.back() != sym::slash() )
					folder_.push_back( sym::backslash() );
			} else
				folder_.clear();
		}
	}
	/**
	 * @brief Проверяет является ли путь уже перенаправленым
	 * @param file Путь для проверки
	 * @return true, если путь ведет в оверлей
	 */
	bool isAlreadyRedirected( const CharType *file ) {
		if ( !file || !*file || file[1] != sym::colon() ) return false;
		auto relPath = getRelPath( file );
		if ( relPath == file ) return false;

		for ( auto &&alt_path : altPaths_ ) {
			if ( strstr( relPath, alt_path.c_str() ) != relPath ) continue;
			return true;
		}

		return false;
	}
	/**
	 * @brief Получает полный путь к файлу
	 * @param file Файл
	 * @return В случае ошибки возвращает переданный путь, иначе полный
	 */
	std::basic_string<CharType> getFullPath( const CharType *file ) {
		if ( !file || ( *file && file[1] == sym::colon() ) ) return file;
		if constexpr ( std::is_same<CharType, char>::value ) {
			if ( !std::strcmp( file, "." ) || !std::strcmp( file, ".." ) || !std::strcmp( file, "CONIN$" ) ||
				 !std::strcmp( file, "CONOUT$" ) || !std::strcmp( file, "NUL" ) )
				return file;
		} else {
			if ( !std::wcscmp( file, L"." ) || !std::wcscmp( file, L".." ) || !std::wcscmp( file, L"CONIN$" ) ||
				 !std::wcscmp( file, L"CONOUT$" ) || !std::wcscmp( file, L"NUL" ) )
				return file;
		}
		return currentFolder_ + file;
	}


	/**
	 * @brief Process hook function
	 * @tparam Result Type of function result
	 * @tparam OrigFunc Type of function to call original (must contain argument `const CharType*`)
	 * @tparam CheckResultFunc Type of function to is result valid (must contain argument `Result`)
	 * @param file File to redirect
	 * @param origFunc Original function
	 * @param checkResult Function to check result
	 * @param allowCreate разрешить создание. Если разрешено, то альтернативный путь возвращается если каталог существует
	 * @return Result
	 */
	template<typename Result, typename OrigFunc, typename CheckResultFunc>
	Result processHook( const CharType *file,
						const OrigFunc &origFunc,
						const CheckResultFunc &checkResult,
						const char *name = nullptr,
						bool allowCreate = false ) {
		auto alt_file = getAltPath( file, allowCreate );
		if ( alt_file.empty() ) return origFunc( file );

		auto result = origFunc( alt_file.c_str() );

		auto relPath = getRelPath( file );
		if ( strstr( relPath, alt_file.c_str() ) == nullptr ) {
			alt_file.erase( 0, root_.length() );
			std::string alt, rel;
			if constexpr ( std::is_same<CharType, char>::value ) {
				alt = alt_file.c_str();
				rel = relPath;
			} else {
				alt = MultiGameFS::toString( alt_file.c_str() );
				rel = MultiGameFS::toString( relPath );
			}
			if ( checkResult( result ) ) {
				if ( name && *name )
					log() << "redirect " << name << " \"" << rel << "\" to \"" << alt << "\"" << std::endl;
				else
					log() << "redirect \"" << rel << "\" to \"" << alt << "\"" << std::endl;
			} else {
				result = origFunc( file );
				if ( checkResult( result ) ) {
					if ( name && *name )
						log() << "failed redirect " << name << " \"" << rel << "\" to \"" << alt << "\"!" << std::endl;
					else
						log() << "failed redirect \"" << rel << "\" to \"" << alt << "\"!" << std::endl;
				}
			}
		}

		return result;
	}

	/**
	 * @brief Process hook iterator function
	 * @tparam OrigFunc Type of function to call original (must contain argument `const CharType*`)
	 * @param file File to redirect
	 * @param origFunc Original function
	 * @return HANDLE of iteration
	 */
	template<typename OrigFunc> HANDLE processIterHook( const CharType *file, const OrigFunc &origFunc ) {
		auto search_path = getSearchPathForIter( file );
		if ( search_path.length() >= 2 && search_path[1] == sym::colon() ) return origFunc( file );
		if ( search_path.length() >= 2 && search_path[0] == sym::dot() &&
			 ( search_path[1] == sym::backslash() || search_path[1] == sym::slash() ) )
			search_path.erase( 0, 2 );

		std::basic_string<CharType> root = root_;
		if ( root.back() != sym::backslash() && root.back() != sym::slash() ) root.push_back( sym::backslash() );

		DWORD err = 0;
		for ( auto &&alt_path : altPaths_ ) {
			std::basic_string<CharType> path = root + alt_path;
			path += search_path;
			auto result = origFunc( path.c_str() );
			if ( result && result != INVALID_HANDLE_VALUE ) {
				path.erase( 0, root_.length() );
				if constexpr ( std::is_same<CharType, char>::value )
					log() << "redirect iterator \"" << file << "\" to \"" << path << "\"" << std::endl;
				else
					log() << "redirect iterator \"" << MultiGameFS::toString( file ) << "\" to \"" << MultiGameFS::toString( path ) << "\""
						  << std::endl;
				return result;
			}
			err = GetLastError();
		}

		auto result = origFunc( file );
		if ( result && result != INVALID_HANDLE_VALUE && err != ERROR_FILE_NOT_FOUND && err != ERROR_PATH_NOT_FOUND ) {
			if constexpr ( std::is_same<CharType, char>::value )
				log() << "failed redirect iterator \"" << file << "\" " << std::hex << err << std::endl;
			else
				log() << "failed redirect iterator \"" << MultiGameFS::toString( file ) << "\" " << std::hex << err << std::endl;
		}
		return result;
	}

	static const CharType *strstr( const CharType *str, const CharType *sub ) {
		if constexpr ( std::is_same<CharType, char>::value ) {
			return std::strstr( str, sub );
		} else {
			return std::wcsstr( str, sub );
		}
	}

	static int strcmp( const CharType *str, const CharType *sub ) {
		if constexpr ( std::is_same<CharType, char>::value ) {
			return std::strcmp( str, sub );
		} else {
			return std::wcscmp( str, sub );
		}
	}

	struct sym {
		static CharType dot() {
			if constexpr ( std::is_same<CharType, char>::value ) {
				return '.';
			} else {
				return L'.';
			}
		}
		static CharType colon() {
			if constexpr ( std::is_same<CharType, char>::value ) {
				return ':';
			} else {
				return L':';
			}
		}
		static CharType slash() {
			if constexpr ( std::is_same<CharType, char>::value ) {
				return '/';
			} else {
				return L'/';
			}
		}
		static CharType backslash() {
			if constexpr ( std::is_same<CharType, char>::value ) {
				return '\\';
			} else {
				return L'\\';
			}
		}
		static CharType null() {
			if constexpr ( std::is_same<CharType, char>::value ) {
				return '\0';
			} else {
				return L'\0';
			}
		}
		static CharType space() {
			if constexpr ( std::is_same<CharType, char>::value ) {
				return ' ';
			} else {
				return L' ';
			}
		}
		static CharType tab() {
			if constexpr ( std::is_same<CharType, char>::value ) {
				return '\t';
			} else {
				return L'\t';
			}
		}
	};
};

