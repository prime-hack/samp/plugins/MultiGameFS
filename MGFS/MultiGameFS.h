#pragma once

#include <map>
#include <string>
#include <ostream>
#include <fstream>
#include <memory>
#include <vector>

template<typename T> class MultiGameFSTempl;

class MultiGameFS {
	std::unique_ptr<MultiGameFSTempl<char>> fsa_;
	std::unique_ptr<MultiGameFSTempl<wchar_t>> fsw_;

	void *hooksCtx_ = nullptr;
	/// папка с игрой
	char root_[260]{ 0 };
	/// трассировка вызовов
	bool trace_ = false;

public:
	MultiGameFS( std::ostream &out, std::string_view modPath, std::string_view defaultPath, bool trace = false );
	~MultiGameFS();

	enum
	{
		ALT_PATH_MOD = 0,
		ALT_PATH_DEFAULT,

		ALT_PATH_COUNT
	};

	std::vector<std::string> alt_paths_;

	/**
	 * @brief Устанавливает редирект по имени
	 * @details например для чтения arz_mouse.png вместо mouse.png
	 * @param orig имя оригинального файла
	 * @param alt имя в которое будет осуществлен редирект
	 */
	void setNameRedirect( const char *orig, const char *alt );

protected:
	std::ostream &out_;
	std::ofstream trace_out_;

	std::ostream &log();

private:
	/**
	 * @brief Получает путь относительно папки игры
	 * @param fullPath полный путь
	 * @return путь относительно папки с игрой. Никогда не возвращает nullptr
	 */
	const char *getRelPath( const char *fullPath );

public:
	/**
	 * @brief Преобразовывает юникод в многобайтовую кодировку
	 * @param str юникод-втрока
	 * @return строка в многобайтовой кодировке
	 */
	static std::string toString( std::wstring_view str );
	/**
	 * @brief Преобразовывает многобайтовую кодировку в юникод
	 * @param str строка
	 * @return строка в юникоде
	 */
	static std::wstring toWstring( std::string_view str );


	template<typename R, typename... Args> static R stdCallOriginal( R ( *original )( Args... ), Args... args ) {
		auto ptr = (void *)original;
		auto original_ = (R( __stdcall * )( Args... ))ptr;
		return original_( args... );
	}
};
