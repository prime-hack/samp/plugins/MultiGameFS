#include "MultiGameFS.h"

#include <llmo/fn.h>

#include <io.h>

#include <windows.h>

#include "MultiGameFSTempl.hpp"

MultiGameFS::MultiGameFS( std::ostream &out, std::string_view modPath, std::string_view defaultPath, bool trace ) :
	out_( out ), trace_( trace ) {
	alt_paths_.push_back( std::string( modPath ) + "\\" );
	alt_paths_.push_back( std::string( defaultPath ) + "\\" );
	getcwd( root_, sizeof( root_ ) );
	std::strcat( root_, "\\" );
	log() << "root - " << root_ << std::endl;

	if ( trace_ ) {
		trace_out_.open( "MultiGameFS.trace" );
		if ( !trace_out_.is_open() ) {
			log() << "Can't create trace file. Trace is disabled" << std::endl;
			trace_ = false;
		}
	}

	hooksCtx_ = llmo::mem::allocate();
	memset( hooksCtx_, 0xCC, llmo::mem::pageSize() );
	log() << "Hooks context addr: " << std::hex << hooksCtx_ << " size: " << std::hex << llmo::mem::pageSize() << std::endl;

	fsa_ = std::make_unique<MultiGameFSTempl<char>>( out_, root_, trace_out_, (std::uint8_t *)hooksCtx_, trace_, alt_paths_ );
	fsw_ = std::make_unique<MultiGameFSTempl<wchar_t>>( out_,
														toWstring( root_ ),
														trace_out_,
														(std::uint8_t *)hooksCtx_ + ( llmo::mem::pageSize() / 2 ),
														trace_,
														alt_paths_ );
}

MultiGameFS::~MultiGameFS() {
	fsw_.reset( nullptr );
	fsa_.reset( nullptr );
	llmo::mem::deallocate( hooksCtx_ );
	if ( trace_out_.is_open() ) trace_out_.close();
}

void MultiGameFS::setNameRedirect( const char *orig, const char *alt ) {
	fsa_->setNameRedirect( orig, alt );
	fsw_->setNameRedirect( toWstring( orig ).c_str(), toWstring( alt ).c_str() );
}

std::ostream &MultiGameFS::log() {
	return out_ << "MultiGameFS: ";
}

const char *MultiGameFS::getRelPath( const char *fullPath ) {
	const char *relPath = fullPath;
	if ( std::strstr( fullPath, root_ ) == fullPath ) {
		relPath = &fullPath[strlen( root_ )];
		if ( *relPath == '\\' || *relPath == '/' ) relPath++;
	}

	return relPath;
}

std::string MultiGameFS::toString( std::wstring_view str ) {
	auto len = WideCharToMultiByte( CP_ACP, 0, str.data(), str.length(), nullptr, 0, nullptr, nullptr );
	std::string result( len, '\0' );
	WideCharToMultiByte( CP_ACP, 0, str.data(), str.length(), result.data(), result.length(), nullptr, nullptr );
	return result;
}

std::wstring MultiGameFS::toWstring( std::string_view str ) {
	auto len = MultiByteToWideChar( CP_ACP, 0, str.data(), str.length(), nullptr, 0 );
	std::wstring result( len, '\0' );
	MultiByteToWideChar( CP_ACP, 0, str.data(), str.length(), result.data(), result.length() );
	return result;
}
