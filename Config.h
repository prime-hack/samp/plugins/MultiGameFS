#pragma once

#include <ostream>
#include <map>
#include <string>

class Config {
	static constexpr auto kConfigFile = "MultiGameFS.cfg";
	static constexpr auto kDefaultFSKey = "defaultFS";
	static constexpr auto kTraceKey = "trace";
	static constexpr auto kHotReloadKey = "hotReloadMP";

public:
	explicit Config( std::ostream &out );
	~Config();

	[[nodiscard]] std::string_view getDefaultFS() const;
	[[nodiscard]] bool traceEnabled() const;
	[[nodiscard]] bool hotReloadMP() const;

	[[nodiscard]] std::string_view getFromStorage( const std::string &key, bool autoInserModPath = false ) const;

protected:
	std::ostream &out;
	mutable std::map<std::string, std::string> storage;
	std::string defaultFS = "mods";
	bool trace = false;
	bool hotReload = false;

	std::ostream &log();
};
