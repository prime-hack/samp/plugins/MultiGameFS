#include "MGFSLoader.h"
#include "Config.h"
#include "MGFS/MultiGameFS.h"

#include <chrono>
#include <iomanip>

#include <llmo/mem/mem.h>

#include <windows.h>

[[maybe_unused]] MGFSLoader g_instance;

MGFSLoader::MGFSLoader() noexcept {
	out_.open( "MultiGameFS.log" );
	if ( !out_.is_open() ) return; // Can't work without write access

	log() << "Command line - " << GetCommandLineA() << std::endl;
	detectServer();

	try {
		config_ = std::make_unique<Config>( out_ );
	} catch ( std::exception &exception ) { log() << "Can't create Config instance: " << exception.what() << std::endl; }

	if ( config_ ) {
		std::string_view fs;
		try {
			fs = config_->getFromStorage( server_ );
			if ( !makePath( fs ) ) fs = config_->getDefaultFS();
			if ( !makePath( fs ) ) {
				log() << "Invalid FS path \"" << fs << "\"" << std::endl;
				return;
			}
		} catch ( std::exception &exception ) {
			log() << "Can't make FS: " << exception.what() << std::endl;
			return;
		}

		if ( restartGame( fs ) ) ExitProcess( 0 );

		try {
			log() << "Mod path - " << fs << std::endl;
			if ( fs != config_->getDefaultFS() ) log() << "Default mod path - " << config_->getDefaultFS() << std::endl;
			mgfs_ = std::make_unique<MultiGameFS>( out_, fs, config_->getDefaultFS(), config_->traceEnabled() );
		} catch ( std::exception &exception ) { log() << "Can't create MultiGameFS instance: " << exception.what() << std::endl; }
		loadAsiPlugins( fs );
		if ( fs != config_->getDefaultFS() ) loadAsiPlugins( config_->getDefaultFS() );
	}
}

MGFSLoader::~MGFSLoader() {
	mgfs_.reset( nullptr );
	config_.reset( nullptr );
	log() << "Unload plugin" << std::endl;
	out_.close();
}

void MGFSLoader::detectServer() noexcept {
	for ( auto &&vcmplibName : kMpLibNames ) {
		mplib_ = GetModuleHandleW( vcmplibName );
		if ( mplib_ && mplib_ != INVALID_HANDLE_VALUE ) {
			mplibName_ = vcmplibName;
			break;
		}
	}
	if ( mplib_ && mplib_ != INVALID_HANDLE_VALUE ) {
		auto mpName = getMPName();
		if ( mpName == "LU" )
			server_ = findArgument( "-ip" );
		else
			server_ = findArgument( "-h" );
		if ( !server_.empty() ) {
			std::string_view port;
			if ( mpName == "LU" )
				port = findArgument( "-port" );
			else
				port = findArgument( "-p" );
			if ( !port.empty() ) {
				server_ += ":";
				server_ += port;
			}
			log() << "Detect " << mpName << " server - " << server_ << std::endl;
		}
	}
}

std::string MGFSLoader::getMPName() noexcept {
	auto stem = mplibName_.stem().string();
	auto hyphenPos = stem.find( '-' );
	if ( hyphenPos != std::string::npos ) stem.erase( hyphenPos );
	std::transform( stem.begin(), stem.end(), stem.begin(), ::toupper );
	return stem;
}

std::string_view MGFSLoader::findArgument( std::string_view key ) noexcept {
	static auto args = (const std::string_view)GetCommandLineA();

	std::size_t startPos = 0;
	auto keyPos = std::string_view::npos;
	while ( true ) {
		keyPos = args.find( key, startPos );
		if ( keyPos == std::string_view::npos || keyPos + key.length() >= args.size() ) return "";
		if ( key.back() != ' ' && key.back() != '\t' && args[keyPos + key.length()] != ' ' && args[keyPos + key.length()] != '\t' ) {
			startPos = keyPos + 1;
			continue;
		}
		if ( keyPos == 0 || key.front() == ' ' || key.front() == '\t' || args[keyPos - 1] == ' ' || args[keyPos - 1] == '\t' ) break;
		startPos = keyPos + 1;
	}

	auto argPos = keyPos + key.length();
	while ( argPos < args.length() && ( args[argPos] == ' ' || args[argPos] == '\t' ) ) ++argPos;
	if ( argPos == args.length() ) return "";

	auto argEnd = args.find( ' ', argPos );
	if ( argEnd == std::string_view::npos ) argEnd = args.find( '\t', argPos );

	std::string_view result;
	try {
		if ( argEnd != std::string_view::npos )
			result = args.substr( argPos, argEnd - argPos );
		else
			result = args.substr( argPos );
	} catch ( std::exception &exception ) {
		g_instance.log() << "std::string_view::substr() is cause exception: " << exception.what() << std::endl;
	}

	return result;
}

bool MGFSLoader::makePath( const std::filesystem::path &path ) {
	if ( path.empty() || path == "." || path == ".." ) return false;

	if ( !std::filesystem::exists( path ) || !std::filesystem::is_directory( path ) ) {
		if ( std::filesystem::exists( path ) && !std::filesystem::is_directory( path ) && !std::filesystem::remove( path ) ) {
			g_instance.log() << "Mod path \"" << path << "\" is not a directory, but this can't be deleted" << std::endl;
			return false;
		}
		if ( !std::filesystem::create_directory( path ) ) {
			g_instance.log() << "Can't create mod path \"" << path << "\"" << std::endl;
			return false;
		}
	}

	return true;
}

bool MGFSLoader::restartGame( const std::filesystem::path &fs ) {
	auto exeFile = getModulePath();
	if ( exeFile.parent_path() != std::filesystem::current_path() ) {
		log() << "Game restarted with modded exe - " << exeFile << std::endl;
		return false;
	}
	if ( !mplibName_.empty() ) {
		auto mpPath = getModulePath( mplibName_.c_str() );
		if ( mpPath.parent_path() != std::filesystem::current_path() ) {
			log() << "Game restarted with modded MP - " << mpPath << std::endl;
			return false;
		}
	}

	auto gtaMod = findInMod( fs, exeFile.filename().c_str() );
	auto mpMod = findInMod( fs, mplibName_.c_str() );

	if ( gtaMod.empty() && !mpMod.empty() && mplibName_ != L"vcmp-proxy.dll" && config_ && config_->hotReloadMP() ) {
		log() << "Reload " << getMPName() << std::endl;
		if ( FreeLibrary( (HMODULE)mplib_ ) ) {
			if ( ( mpMod.filename() == L"samp.dll" || // Fix SA:MP reloading for gta_sa.exe 1.0 US
				   mpMod.filename() == L"crmp.dll" ) &&
				 !memcmp( (void *)0x747483, "\x90\x90\x90\x90\x90\x90", 6 ) ) {
				auto prot = llmo::mem::prot::get( 0x747483, 5 );
				llmo::mem::prot::set( 0x747483, 5 );
				memcpy( (void *)0x747483, "\x89\x35\xC0\xD4\xC8\x00", 6 );
				llmo::mem::prot::set( 0x747483, 5, prot );
			}
			mplib_ = LoadLibraryW( mpMod.c_str() );
			if ( mplib_ && mplib_ != INVALID_HANDLE_VALUE ) return false;
		}
	}

	if ( !gtaMod.empty() || !mpMod.empty() ) {

		if ( gtaMod.empty() ) gtaMod = exeFile;

		STARTUPINFOW StartupInfo;
		memset( &StartupInfo, 0, sizeof( STARTUPINFOW ) );
		StartupInfo.cb = sizeof( StartupInfo );
		PROCESS_INFORMATION ProcessInfo;
		memset( &ProcessInfo, 0, sizeof( PROCESS_INFORMATION ) );
		CreateProcessW( gtaMod.c_str(),
						GetCommandLineW(),
						nullptr,
						nullptr,
						0,
						DETACHED_PROCESS | CREATE_SUSPENDED,
						nullptr,
						nullptr,
						&StartupInfo,
						&ProcessInfo );
		if ( ProcessInfo.hProcess != nullptr ) {
			if ( mplib_ && mplib_ != INVALID_HANDLE_VALUE ) {
				if ( mpMod.empty() ) mpMod = mplibName_;
				inject( ProcessInfo.dwProcessId, mpMod.c_str() );
			}
			ResumeThread( ProcessInfo.hThread );
			return true;
		}
	}
	return false;
}

std::filesystem::path MGFSLoader::findInMod( const std::filesystem::path &modpath, std::wstring_view filename ) noexcept {
	if ( filename.empty() ) return "";

	auto path = std::filesystem::path( modpath );
	if ( std::filesystem::exists( path / filename ) ) return ( path / filename ).string();

	if ( !config_ || config_->getDefaultFS() == modpath ) return "";

	path = std::filesystem::path( config_->getDefaultFS() );
	if ( std::filesystem::exists( path / filename ) ) return ( path / filename ).string();

	return "";
}

void MGFSLoader::loadAsiPlugins( const std::filesystem::path &modpath ) {
	if ( !std::filesystem::exists( modpath ) || !std::filesystem::is_directory( modpath ) ) return;

	auto current_path = std::filesystem::current_path();
	for ( auto &&file : std::filesystem::directory_iterator( modpath ) ) {
		if ( file.path().extension() != ".asi" ) continue;
		log() << "Load modded asi " << file.path().filename();

		std::filesystem::current_path( current_path / modpath );
		auto hlib = LoadLibraryA( file.path().filename().string().c_str() );
		std::filesystem::current_path( current_path );

		if ( hlib && hlib != INVALID_HANDLE_VALUE )
			out_ << " at " << std::hex << hlib << std::endl;
		else
			out_ << " failed " << std::hex << GetLastError() << std::endl;
	}
}

bool MGFSLoader::inject( size_t pId, const wchar_t *dllname ) {
	HANDLE h = OpenProcess( PROCESS_ALL_ACCESS, FALSE, pId );
	if ( h ) {
		auto LoadLibAddr = (LPVOID)GetProcAddress( GetModuleHandleA( "kernel32.dll" ), "LoadLibraryW" );
		auto stringSize = wcslen( dllname ) * sizeof( wchar_t );
		auto dereercomp = VirtualAllocEx( h, nullptr, stringSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
		WriteProcessMemory( h, dereercomp, dllname, stringSize, nullptr );
		HANDLE asdc = CreateRemoteThread( h, nullptr, 0, (LPTHREAD_START_ROUTINE)LoadLibAddr, dereercomp, 0, nullptr );
		WaitForSingleObject( asdc, INFINITE );
		VirtualFreeEx( h, dereercomp, stringSize, MEM_RELEASE );
		CloseHandle( asdc );
		CloseHandle( h );
		return true;
	}
	return false;
}

std::filesystem::path MGFSLoader::getModulePath( std::wstring_view module ) {
	HMODULE instance = nullptr;
	if ( !module.empty() ) instance = GetModuleHandleW( module.data() );
	wchar_t path[MAX_PATH]{ 0 };
	GetModuleFileNameW( instance, path, MAX_PATH );
	return path;
}

std::ostream &MGFSLoader::log() {
	return out_ << "Loader: ";
}
