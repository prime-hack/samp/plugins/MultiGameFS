#include "Config.h"
#include <fstream>

Config::Config( std::ostream &out ) : out( out ) {
	log() << "load" << std::endl;
	std::ifstream cfg( kConfigFile );
	if ( !cfg.is_open() ) {
		log() << "Can't read config file" << std::endl;
		return;
	}
	std::string line;
	while ( std::getline( cfg, line ) ) {
		if ( line.empty() ) continue;
		if ( line.starts_with( '#' ) || line.starts_with( ';' ) || line.starts_with( "//" ) ) continue;
		auto assignPos = line.find( '=' );
		if ( assignPos == 0 || assignPos == std::string::npos || assignPos == line.length() - 1 ) continue;
		auto key = line.substr( 0, assignPos );
		auto value = line.substr( assignPos + 1 );
		try {
			storage.insert( { key, value } );
		} catch ( std::exception &exception ) { log() << "Can't insert config line to storage: " << exception.what() << std::endl; }
		if ( key == kDefaultFSKey ) defaultFS = value;
		if ( key == kTraceKey ) {
			try {
				trace = std::stoi( value );
			} catch ( std::exception &exception ) { log() << "Invalid value for " << kTraceKey << std::endl; }
		}
		if ( key == kHotReloadKey ) {
			try {
				hotReload = std::stoi( value );
			} catch ( std::exception &exception ) { log() << "Invalid value for " << kHotReloadKey << std::endl; }
		}
	}
	cfg.close();
}

Config::~Config() {
	log() << "Save" << std::endl;
	std::ofstream cfg( kConfigFile );
	if ( !cfg.is_open() ) {
		log() << "Can't write config file" << std::endl;
		return;
	}
	try {
		storage.insert( { kDefaultFSKey, defaultFS } );
		storage.insert( { kTraceKey, std::to_string( trace ) } );
		storage.insert( { kHotReloadKey, std::to_string( hotReload ) } );
	} catch ( std::exception &exception ) { log() << "Can't insert configs to storage: " << exception.what() << std::endl; }
	for ( auto &&[key, value] : storage ) { cfg << key << "=" << value << std::endl; }
	cfg.close();
}

std::string_view Config::getDefaultFS() const {
	return defaultFS;
}

bool Config::traceEnabled() const {
	return trace;
}

bool Config::hotReloadMP() const {
	return hotReload;
}

std::string_view Config::getFromStorage( const std::string &key, bool autoInserModPath ) const {
	auto it = storage.find( key );
	if ( it == storage.end() ) {
		if ( autoInserModPath ) {
			storage.insert( { key, defaultFS } );
			return defaultFS;
		}
		return "";
	}
	return it->second;
}

std::ostream &Config::log() {
	return out << "Config: ";
}
