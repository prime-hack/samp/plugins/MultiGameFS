[![pipeline  status](https://gitlab.com/prime-hack/samp/plugins/MultiGameFS/badges/main/pipeline.svg)](https://gitlab.com/prime-hack/samp/plugins/MultiGameFS/-/commits/main)

# MultiGameFS

#### Create FS overlays for game.

### Features

- redirect any game resources
- redirect most multiplayer resources
- redirect multiplayer library
- redirect game executable
- redirect fonts
- redirect libraries
- redirect most asi-plugins resources
- load additional asi-plugins for overlays

### Features that will appear later 

- redirect MVCVRT file operations
- redirect GetModuleHandle(Ex) functions
- redirect ReplaceFile
- redirect SetFileAttributes(Transacted)
- redirect CreateFileTransacted
- redirect CreateHardLink(Transacted)
- redirect CreateSymbolicLink(Transacted)
- redirect DecryptFile, EncryptFile and FileEncryptionStatus
- redirect DefineDosDevice and QueryDosDevice (need for run game in Win98 mode)
- redirect MoveFile(Transacted) and MoveFileWithProgress
- redirect CopyFile2

## Config

Any settings stored in file **MultiGameFS.cfg** like below:

```ini
defaultFS=mods
trace=0
hotReloadMP=0
samp.sr.team:1337=samp.sr.team.1337_mods
```

- **defaultFS** - default folder with overlays
- **trace** - trace function calls (traces saved to file **MultiGameFS.trace**)
- **hotReloadMP** - hot reloading multiplayer library without game restarting
- **samp.sr.team:1337** - folder with overlays for SA:MP server _[samp.sr.team:1337](samp:samp.sr.team:1337)_

## Issues

- **hotReloadMP** doesn't work with Arizlna launcher

## [Download](https://gitlab.com/prime-hack/samp/plugins/MultiGameFS/-/jobs/artifacts/main/download\?job\=win32)
